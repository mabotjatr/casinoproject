package za.co.casinoproject.exception;

public class GenericNotFoundException extends RuntimeException{
    public GenericNotFoundException(Long id){
        super(String.format("Player/Transaction not found!!!!"));
    }

    public GenericNotFoundException(String message){
        super(message);
    }
}
