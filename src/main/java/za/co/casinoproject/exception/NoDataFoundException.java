package za.co.casinoproject.exception;

public class NoDataFoundException extends RuntimeException{

    public NoDataFoundException(){
        super(String.format("No data found!!!"));
    }
}
