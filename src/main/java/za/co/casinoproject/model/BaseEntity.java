package za.co.casinoproject.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@SuperBuilder
public abstract class BaseEntity implements Serializable {
    public BaseEntity() {}
}
