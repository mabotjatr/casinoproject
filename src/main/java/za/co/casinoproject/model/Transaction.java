package za.co.casinoproject.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "TRANSACTION")
public class Transaction  extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    /*@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_player_id", referencedColumnName = "playerId")*/
    private Long playerId;
    private LocalDateTime lastModDate;
    private BigDecimal amount;
    private TransactionType transactionType;
}
