package za.co.casinoproject.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DoTransactionDTO implements Serializable {

    private Long playerId;

    private BigDecimal amount;

    private TransactionType transactionType;
}
