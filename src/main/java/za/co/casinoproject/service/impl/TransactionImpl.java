package za.co.casinoproject.service.impl;

import za.co.casinoproject.model.Player;
import za.co.casinoproject.model.Transaction;

import java.util.List;

public interface TransactionImpl {

    //Transaction operations
    Transaction wagerTransaction(Transaction transaction);
    Transaction winTransaction(Transaction transaction );
    Transaction findTransactionById(Long id);
    void deleteTransaction(Long id);
    List<Transaction> findAllTransactions();

    void deleteAll();
}
