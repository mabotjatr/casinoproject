package za.co.casinoproject.service.impl;

import za.co.casinoproject.model.Player;

import java.util.List;

public interface PlayerServiceImpl {
    //Player operations
    Player savePlayer(Player player);
    Player findPlayerById(Long id);
    void updatePlayer(Player player, Long id);
    void deletePlayer(Long id);
    List<Player> findPlayers();

    void deleteAll();
}
