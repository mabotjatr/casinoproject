package za.co.casinoproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.casinoproject.exception.GenericNotFoundException;
import za.co.casinoproject.exception.NoDataFoundException;
import za.co.casinoproject.model.Account;
import za.co.casinoproject.model.Player;
import za.co.casinoproject.model.Transaction;
import za.co.casinoproject.model.TransactionType;
import za.co.casinoproject.repository.TransactionRepository;
import za.co.casinoproject.service.impl.TransactionImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
//import org.slf4j.*;

@Service
public class TransactionService implements TransactionImpl {


    private final TransactionRepository transactionRepository;


    public TransactionService (TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }


    @Override
    public Transaction wagerTransaction(Transaction transaction) {
        transaction.setTransactionType(TransactionType.WAGER);
        transaction.setLastModDate(LocalDateTime.now());

        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction winTransaction(Transaction transaction) {
        transaction.setTransactionType(TransactionType.WIN);
        transaction.setLastModDate(LocalDateTime.now());
        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction findTransactionById(Long id) {
        return transactionRepository.findById(id).orElseThrow(() -> new GenericNotFoundException(id));
    }

    @Override
    public void deleteTransaction(Long id) {
        transactionRepository.deleteById(id);
    }

    @Override
    public List<Transaction> findAllTransactions() {
        var transactions = (List<Transaction>) transactionRepository.findAll();

        if(transactions.isEmpty()){
            throw new NoDataFoundException();
        }
        return transactions;
    }

    @Override
    public void deleteAll() {
        transactionRepository.deleteAll();
    }
}
