package za.co.casinoproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.casinoproject.exception.GenericNotFoundException;
import za.co.casinoproject.exception.NoDataFoundException;
import za.co.casinoproject.model.*;
import za.co.casinoproject.repository.PlayerRepository;
import za.co.casinoproject.service.impl.PlayerServiceImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class PlayerService implements PlayerServiceImpl {

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private TransactionService transactionService;

    @Override
    public Player savePlayer(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public Player findPlayerById(Long id) {
        return playerRepository.findById(id).orElseThrow(() -> new GenericNotFoundException(id));
    }

    @Override
    public void updatePlayer(Player player, Long id) {
        playerRepository.save(player);
    }

    @Override
    public void deletePlayer(Long id) {
        playerRepository.deleteById(id);
    }

    @Override
    public List<Player> findPlayers() {
        var players = (List<Player>) playerRepository.findAll();
        if(players.isEmpty()){
            throw new NoDataFoundException();
        }
        return players;
    }

    @Override
    public void deleteAll() {
        playerRepository.deleteAll();
    }

    public Player doTransaction ( DoTransactionDTO doTransactionDTO ) {

        var transaction = Transaction.builder()
                .lastModDate(LocalDateTime.now())
                .amount(doTransactionDTO.getAmount())
                .transactionType(doTransactionDTO.getTransactionType())
                .playerId(doTransactionDTO.getPlayerId())
                .build();

        var player = findPlayerById(doTransactionDTO.getPlayerId());
        assert player != null;

        if (doTransactionDTO.getTransactionType().equals(TransactionType.WAGER)) {

            if (!checkPositiveBalance (doTransactionDTO.getPlayerId(), doTransactionDTO.getAmount()))
                throw new GenericNotFoundException("Insufficiant balance in the account");

            var savedTrans = transactionService.wagerTransaction(transaction);

            if (savedTrans != null) {

                var account = player.getAccount();
                var total = account.getBalance().subtract(doTransactionDTO.getAmount());
                account.setBalance(total);
                player.setAccount(account);

                updatePlayer(player, player.getId());
            }

        } else if (doTransactionDTO.getTransactionType().equals(TransactionType.WIN)) {

            var savedTrans =  transactionService.winTransaction(transaction);
            if (savedTrans != null) {
                var account = player.getAccount();
                var total = account.getBalance().add(doTransactionDTO.getAmount());
                account.setBalance(total);
                player.setAccount(account);

                updatePlayer(player, player.getId());
            }

        }else  {
            throw new GenericNotFoundException("Unknown transaction type");
        }

        return findPlayerById(doTransactionDTO.getPlayerId());
    }

    public boolean checkPositiveBalance (Long playId, BigDecimal amount) {
        var player = findPlayerById(playId);
        return player.getAccount().getBalance().compareTo(amount) >= 0;
    }
}
