package za.co.casinoproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import za.co.casinoproject.model.DoTransactionDTO;
import za.co.casinoproject.model.Player;
import za.co.casinoproject.service.PlayerService;

import java.util.List;

@RestController
@RequestMapping("/api/casino")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @PostMapping(value = "/player",
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public Player createPlayer(@RequestBody Player player){
        return playerService.savePlayer(player);
    }


    @GetMapping(value = "/balance/{id}")
    public Player getPlayer(@PathVariable Long id){
        return playerService.findPlayerById(id);
    }


    @GetMapping(value = "/players")
    public List<Player> getAllPlayers(){
        return playerService.findPlayers();
    }


    @PostMapping(value = "/transact",
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public Player doTransaction(@RequestBody DoTransactionDTO transactionDTO){
        return playerService.doTransaction(transactionDTO);
    }

}
