package za.co.casinoproject.repository;

import org.springframework.data.repository.CrudRepository;
import za.co.casinoproject.model.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {
}
