package za.co.casinoproject.repository;

import org.springframework.data.repository.CrudRepository;
import za.co.casinoproject.model.Player;

public interface PlayerRepository extends CrudRepository<Player, Long> {
}
