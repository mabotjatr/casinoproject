package za.co.casinoproject;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import za.co.casinoproject.exception.GenericNotFoundException;
import za.co.casinoproject.exception.NoDataFoundException;
import za.co.casinoproject.model.*;
import za.co.casinoproject.service.PlayerService;
import za.co.casinoproject.service.TransactionService;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CasinoprojectApplicationTests {

    @Autowired
    PlayerService playerService;

    @Autowired
    TransactionService transactionService;

    private Player savedPlayer, player, savedPlayerWithAcc;

    @BeforeAll
    void init() {
        playerService.deleteAll();
        transactionService.deleteAll();

        player = Player.builder()
                .name("John")
                .build();

        savedPlayer = playerService.savePlayer(player);
    }

    @Order(1)
    @Test
    void testCreatePlayer() {

        assertNotNull(savedPlayer);
        assertNotNull(savedPlayer.getId());
        assertEquals(player.getName(), savedPlayer.getName());
        assertNull(player.getAccount());
    }

    @Order(2)
    @Test
    void testFind() {
        //Test find
        var player1 = playerService.findPlayerById(savedPlayer.getId());
        assertNotNull(player1);
        assertNotNull(player1.getId());


        //Test find fail
        final var dummyId = 22L;
        var error = assertThrows(GenericNotFoundException.class,
                () -> playerService.findPlayerById(dummyId));
        assertNotNull(error);
        assertEquals("Player/Transaction not found!!!!", error.getMessage());
    }

    @Order(3)
    @Test
    void testFindAll() {
        //Test find
        var player1 = playerService.findPlayers();
        assertEquals(1, player1.size());

    }

    @Order(4)
    @Test
    void testDelete() {
        assertTrue(true);
    }

    @Order(5)
    @Test
    void testUpdate() {
        assertTrue(true);
    }

    @Order(6)
    @Test
    void testCreatePlayWithAccount() {

        var account = Account.builder()
                .balance(BigDecimal.valueOf(500))
                .build();

        var player = Player.builder()
                .name("Tebogo")
                .account(account)
                .build();

        assertNotNull(player.getAccount());

        savedPlayerWithAcc = playerService.savePlayer(player);
        assertNotNull(savedPlayerWithAcc);
        assertNotNull(savedPlayerWithAcc.getId());

        assertNotNull(savedPlayerWithAcc.getAccount());
        assertEquals(player.getName(), savedPlayerWithAcc.getName());
    }

    @Order(7)
    @Test
    void testTransaction() {

        var error = assertThrows(NoDataFoundException.class,
                () -> transactionService.findAllTransactions());
        assertNotNull(error);
        assertEquals("No data found!!!", error.getMessage());
    }

    @Order(8)
    @Test
    void testCreateTransaction() {

        var dto1 = DoTransactionDTO.builder()
                .transactionType(TransactionType.WAGER)
                .amount(BigDecimal.valueOf(1554))
                .playerId(savedPlayerWithAcc.getId())
                .build();

        var error = assertThrows(GenericNotFoundException.class,
                () -> playerService.doTransaction(dto1));
        assertNotNull(error);
        assertEquals("Insufficiant balance in the account", error.getMessage());


        //Win
        var dto2 = DoTransactionDTO.builder()
                .transactionType(TransactionType.WIN)
                .amount(BigDecimal.valueOf(1554))
                .playerId(savedPlayerWithAcc.getId())
                .build();

        var savedTrans = playerService.doTransaction(dto2);
        assertNotNull(savedTrans);
        assertNotNull(savedTrans.getId());

        final var totalAmt = savedPlayerWithAcc
                .getAccount()
                .getBalance()
                .add(dto2.getAmount());

        assertEquals(0, totalAmt.compareTo(savedTrans
                .getAccount().getBalance()));


        //Wager
        var dto3 = DoTransactionDTO.builder()
                .transactionType(TransactionType.WAGER)
                .amount(BigDecimal.valueOf(500))
                .playerId(savedPlayerWithAcc.getId())
                .build();

        var savedTrans2 = playerService.doTransaction(dto3);
        assertNotNull(savedTrans2);
        assertNotNull(savedTrans2.getId());

        final var totalAmt1 = totalAmt
                .subtract(dto3.getAmount());

        assertEquals(0, totalAmt1.compareTo(savedTrans2
                .getAccount().getBalance()));
    }

}
